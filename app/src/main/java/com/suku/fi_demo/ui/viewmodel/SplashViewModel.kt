package com.suku.fi_demo.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.suku.fi_demo.base.BaseViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel : BaseViewModel() {

    var liveData: MutableLiveData<String> = MutableLiveData()

    init {
        viewModelScope.launch {
            delay(4000)
            updateLiveData()
        }
    }

    private fun updateLiveData() {
        liveData.value = "Launch"
    }
}