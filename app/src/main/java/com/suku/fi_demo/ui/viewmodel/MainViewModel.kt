package com.suku.fi_demo.ui.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.suku.fi_demo.base.BaseViewModel
import com.suku.fi_demo.util.DATE_FORMAT_SIMPLE
import com.suku.fi_demo.util.REGEX_DATE
import com.suku.fi_demo.util.REGEX_PAN
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class MainViewModel : BaseViewModel() {

    var liveDataValidation: MutableLiveData<Boolean> = MutableLiveData()
    var liveData: MutableLiveData<String> = MutableLiveData()

    var fieldValidationPAN = false
    var fieldValidationDate = false

    var panNumber: String? = null
    var date: String? = null

    fun panCardValidation(inputValue: String) {
        panNumber = inputValue
        val pattern = Pattern.compile(REGEX_PAN)
        val matcher = pattern.matcher(inputValue)
        fieldValidationPAN = matcher.matches()
        checkRequiredFields()
    }

    private fun checkRequiredFields() {
        liveDataValidation.value = (fieldValidationPAN && fieldValidationDate)
    }

    @SuppressLint("SimpleDateFormat")
    fun dateValidation(date: String) {
        this.date = date
        var status = false
        if (checkDate(date)) {
            val dateFormat: DateFormat = SimpleDateFormat(DATE_FORMAT_SIMPLE, Locale.getDefault())
            dateFormat.isLenient = false
            status = try {
                val outputValue = dateFormat.parse(date)
                System.currentTimeMillis() > outputValue.time
            } catch (e: Exception) {
                false
            }
        }
        fieldValidationDate = status
        checkRequiredFields()
    }

    private fun checkDate(date: String): Boolean {
        return (date.matches(REGEX_DATE.toRegex()))
    }

    fun onClickNext() {
        //TODO API CALL then SHOW Success MSG
        liveData.value = "PAN NUMBER: $panNumber DATE: $date"
    }
}