package com.suku.fi_demo.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.suku.fi_demo.R
import com.suku.fi_demo.base.BaseActivity
import com.suku.fi_demo.ui.viewmodel.SplashViewModel

@SuppressLint("CustomSplashScreen")
class SplashActivity : BaseActivity() {

    private val splashViewModel: SplashViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        observerLiveData()
    }

    private fun observerLiveData() {
        splashViewModel.liveData.observe(this) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}