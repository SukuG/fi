package com.suku.fi_demo.ui

import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.AllCaps
import android.text.SpannableStringBuilder
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import com.suku.fi_demo.R
import com.suku.fi_demo.base.BaseActivity
import com.suku.fi_demo.databinding.ActivityMainBinding
import com.suku.fi_demo.ui.viewmodel.MainViewModel
import com.suku.fi_demo.util.getColoredString
import com.suku.fi_demo.util.log
import com.suku.fi_demo.util.showToast


class MainActivity : BaseActivity() {

    private var day: String = "00"
    private var month: String = "00"
    private var year: String = "0000"
    private lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        validation()
        observerLiveData()
        setLearnMoreText()
        listeners()
    }

    private fun listeners() {
        binding.btnNext.setOnClickListener {
            mainViewModel.onClickNext()
        }
        binding.noPan.setOnClickListener {
            finish()
        }
    }

    private fun validation() {
        //pan card validation
        binding.etPanNumber.filters = arrayOf<InputFilter>(AllCaps())
        binding.etPanNumber.doOnTextChanged { text, _, _, _ ->
            mainViewModel.panCardValidation(text.toString())
        }
        //dob validation
        binding.dobDay.doOnTextChanged { text, _, _, _ ->
            day = text.toString()
            mainViewModel.dateValidation("$day/$month/$year")
            //requestFocus
            if (text?.length == 2)
                binding.dobMonth.requestFocus()
        }
        binding.dobMonth.doOnTextChanged { text, _, _, _ ->
            month = text.toString()
            mainViewModel.dateValidation("$day/$month/$year")
            //requestFocus
            if (text?.length == 2)
                binding.dobYear.requestFocus()
        }
        binding.dobYear.doOnTextChanged { text, _, _, _ ->
            year = text.toString()
            mainViewModel.dateValidation("$day/$month/$year")
        }
    }

    private fun observerLiveData() {
        //pan card validation observer
        mainViewModel.liveDataValidation.observe(this) {
            enableDisableNextButton(it)
        }
        //DOB validation observer
        mainViewModel.liveData.observe(this) {
            log(TAG, it)
            showToast(getString(R.string.success_details_submitted))
            finish()
        }
    }

    //enable/ disable according to status
    private fun enableDisableNextButton(flag: Boolean) {
        binding.btnNext.isEnabled = flag
        binding.btnNext.isClickable = flag
    }

    private fun setLearnMoreText() {
        val finalReturnValue = SpannableStringBuilder()
        finalReturnValue.append(
            getColoredString(
                getString(R.string.learn_more_description),
                ContextCompat.getColor(this, R.color.caption_title_color)
            )
        )
        finalReturnValue.append(
            getColoredString(
                "Learn more",
                ContextCompat.getColor(this, R.color.bg_date_select_color)
            )
        )
        binding.learnMore.text = finalReturnValue
    }
}