package com.suku.fi_demo.base

import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {
    var TAG = javaClass.simpleName
}