package com.suku.fi_demo.base

import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {
    var TAG = javaClass.simpleName
}