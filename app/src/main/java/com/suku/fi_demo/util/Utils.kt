package com.suku.fi_demo.util

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.widget.Toast

const val REGEX_PAN = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
const val REGEX_DATE = "(0?[1-9]|[12][0-9]|3[01])\\/(0?[1-9]|1[0-2])\\/([0-9]{4})"
const val DATE_FORMAT_SIMPLE = "dd/MM/yyyy"

fun log(tag: String, message: String) {
    Log.d(tag, message)
}

fun Context.showToast(message: String) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun getColoredString(mString: String?, colorId: Int): Spannable {
    val spannable: Spannable = SpannableString(mString)
    spannable.setSpan(
        ForegroundColorSpan(colorId),
        0,
        spannable.length,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    return spannable
}